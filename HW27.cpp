﻿#include <iostream>
#include <string>

class Stack
{
	int* stack = new int[0];
	int size = 0;
	bool notEmpty = false;

public:

	int Pop()
	{
		if (size)
		{
			size -= 1;
			return stack[size];
		}

	}

	void Push(int newElement)
	{
		stack;
		stack[size] = newElement;
		size += 1;
	}

	bool ChangeEmpty()
	{
		return size;
	}

	void End()
	{
		stack = nullptr;
	}
};

int main()
{
	using namespace std;
	string comand = "";
	int element;
	Stack myStack;
	cout << "Help\n";
	cout << "pop - get last element\n";
	cout << "push - add new element\n";
	cout << "end - end program execution\n";
	while (comand != "end")
	{
		cout << "Enter comand:\n";
		cin >> comand;
		if (comand == "pop")
		{
			if (myStack.ChangeEmpty()) 
			{
				element = myStack.Pop();
				cout << "Get element: " << element << "\n";
			}
			else
			{
				std::cout << "Stack is empty!\n";
			}
		}
		if (comand == "push")
		{
			cout << "Enter new element:\n";
			cin >> element;
			myStack.Push(element);
			cout << "Add new element: " << element << "\n";
		}
	}
	myStack.End();
}
